package com.example.ajnatermin.ui.theme

import androidx.compose.material3.Typography
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.Shadow
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.sp
import com.example.ajnatermin.R

// Set of Material typography styles to start with
val Typography = Typography(
    bodyLarge = TextStyle(
        fontFamily = FontFamily(Font(R.font.oswald)), // Use the custom font
        fontWeight = FontWeight.Bold,
        fontSize = 30.sp,
        lineHeight = 24.sp,
        letterSpacing = 0.5.sp,
        shadow = Shadow(color = Color.Black, offset = Offset.Zero)
    ),
    bodyMedium = TextStyle(
        fontFamily = FontFamily(Font(R.font.oswald)), // Use the custom font
        fontWeight = FontWeight.Light,
        fontSize = 26.sp,
        lineHeight = 24.sp,
        letterSpacing = 0.5.sp
    ),
    bodySmall = TextStyle(
        fontFamily = FontFamily(Font(R.font.oswald)), // Use the custom font
        fontWeight = FontWeight.ExtraLight,
        fontSize = 13.sp,
        letterSpacing = 0.5.sp
    )

    // Define other text styles as needed
)