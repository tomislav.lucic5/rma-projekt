@file:OptIn(ExperimentalMaterial3Api::class)

package com.example.ajnatermin
import android.app.TimePickerDialog
import android.content.ContentValues.TAG
import android.content.Context
import android.os.Bundle
import android.util.Log
import android.widget.DatePicker
import android.widget.Toast
import androidx.activity.ComponentActivity
import androidx.activity.compose.BackHandler
import androidx.activity.compose.setContent
import androidx.compose.animation.Crossfade
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.combinedClickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.heightIn
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.ClickableText
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material.icons.filled.Create
import androidx.compose.material.icons.filled.DateRange
import androidx.compose.material.icons.filled.Email
import androidx.compose.material.icons.filled.ExitToApp
import androidx.compose.material.icons.filled.List
import androidx.compose.material.icons.filled.Lock
import androidx.compose.material.icons.filled.Search
import androidx.compose.material3.AlertDialog
import androidx.compose.material3.BottomAppBar
import androidx.compose.material3.Button
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.Divider
import androidx.compose.material3.DropdownMenuItem
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.ExposedDropdownMenuBox
import androidx.compose.material3.ExposedDropdownMenuDefaults
import androidx.compose.material3.FloatingActionButton
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.material3.TextField
import androidx.compose.material3.TopAppBar
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.painter.Painter
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.AnnotatedString
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.buildAnnotatedString
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.text.input.VisualTransformation
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextDecoration
import androidx.compose.ui.text.withStyle
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.ajnatermin.screens.LoadingScreen
import com.example.ajnatermin.screens.SplashScreen
import com.example.ajnatermin.ui.theme.Typography
import com.google.firebase.Firebase
import com.google.firebase.Timestamp
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.auth.auth
import com.google.firebase.firestore.DocumentReference
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.toObject
import kotlinx.coroutines.delay
import java.text.SimpleDateFormat
import java.util.Calendar
import java.util.Locale


class MainActivity : ComponentActivity() {

    private val auth: FirebaseAuth by lazy { Firebase.auth }
    private val db: FirebaseFirestore by lazy { FirebaseFirestore.getInstance() }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            AppContent(auth)
        }
    }

    @Composable
    fun AppContent(auth: FirebaseAuth) {
        var showSplashScreen by remember { mutableStateOf(true) }

        LaunchedEffect(showSplashScreen) {
            delay(2000)
            showSplashScreen = false
        }

        Crossfade(targetState = showSplashScreen, label = "") { isSplashScreenVisible ->
            if (isSplashScreenVisible) {
                SplashScreen {
                    showSplashScreen = false
                }
            } else {
                AuthOrMainScreen(auth)
            }
        }
    }

    @Composable
    fun AuthOrMainScreen(auth: FirebaseAuth) {
        var user by remember { mutableStateOf(auth.currentUser) }

        if (user == null) {
            AuthScreen(
                onSignedIn = { signedInUser ->
                    user = signedInUser
                }
            )
        } else {
            MainScreen(
                onSignOut = {
                    auth.signOut()
                    user = null
                },
            )
        }
    }

    @Composable
    fun AuthScreen(onSignedIn: (FirebaseUser) -> Unit) {
        var email by remember { mutableStateOf("") }
        var password by remember { mutableStateOf("") }
        var firstName by remember { mutableStateOf("") }
        var lastName by remember { mutableStateOf("") }
        var isSignIn by remember { mutableStateOf(true) }
        var isPasswordVisible by remember { mutableStateOf(false) }
        var myErrorMessage by remember { mutableStateOf<String?>(null) }

        val imagePainter: Painter = painterResource(id = R.drawable.background)
        val logoPainter: Painter = painterResource(id = R.drawable.logo2)

        Box(
            modifier = Modifier
                .fillMaxSize()
        ) {
            Image(
                painter = imagePainter,
                contentDescription = null,
                modifier = Modifier.fillMaxSize(),
                contentScale = ContentScale.Crop
            )

            Card(
                modifier = Modifier
                    .fillMaxSize()
                    .background(MaterialTheme.colorScheme.surface.copy(alpha = 0.25f))
                    .padding(25.dp)
                    .clip(RoundedCornerShape(16.dp)),
                elevation = CardDefaults.cardElevation()
            ) {
                Column(
                    modifier = Modifier
                        .fillMaxSize()
                        .padding(16.dp),
                    horizontalAlignment = Alignment.CenterHorizontally,
                    verticalArrangement = Arrangement.Center
                ) {
                    Image(
                        painter = logoPainter,
                        contentDescription = null,
                        modifier = Modifier
                            .padding(bottom = 16.dp)
                            .align(Alignment.CenterHorizontally)
                            .size(width = 200.dp, height = 200.dp)
                    )

                    if (!isSignIn) {
                        Spacer(modifier = Modifier.height(8.dp))

                        TextField(
                            value = firstName,
                            onValueChange = { firstName = it },
                            modifier = Modifier
                                .fillMaxWidth()
                                .padding(8.dp),
                            label = {
                                Text("Ime")
                            },
                        )

                        Spacer(modifier = Modifier.height(8.dp))

                        TextField(
                            value = lastName,
                            onValueChange = { lastName = it },
                            modifier = Modifier
                                .fillMaxWidth()
                                .padding(8.dp),
                            label = {
                                Text("Prezime")
                            },
                        )
                    }

                    Spacer(modifier = Modifier.height(16.dp))

                    TextField(
                        value = email,
                        onValueChange = { email = it },
                        modifier = Modifier
                            .fillMaxWidth()
                            .padding(8.dp),
                        label = {
                            Text("Email")
                        },
                        leadingIcon = {
                            Icon(Icons.Default.Email, contentDescription = null)
                        },
                        keyboardOptions = KeyboardOptions.Default.copy(
                            keyboardType = KeyboardType.Email
                        ),
                        visualTransformation = if (isSignIn) VisualTransformation.None else VisualTransformation.None
                    )

                    Spacer(modifier = Modifier.height(8.dp))
                    TextField(
                        value = password,
                        onValueChange = { password = it },
                        modifier = Modifier
                            .fillMaxWidth()
                            .padding(8.dp),
                        label = {
                            Text("Lozinka")
                        },
                        leadingIcon = {
                            Icon(Icons.Default.Lock, contentDescription = null)
                        },
                        visualTransformation = if (isPasswordVisible) VisualTransformation.None else PasswordVisualTransformation(),
                        keyboardOptions = KeyboardOptions.Default.copy(
                            keyboardType = KeyboardType.Password
                        ),
                        trailingIcon = {
                            IconButton(
                                onClick = { isPasswordVisible = !isPasswordVisible }
                            ) {
                                val icon = if (isPasswordVisible) Icons.Default.Lock else Icons.Default.Search
                                Icon(
                                    imageVector = icon,
                                    contentDescription = "Toggle Password Visibility"
                                )
                            }
                        }
                    )

                    Spacer(modifier = Modifier.height(16.dp))

                    if (myErrorMessage != null) {
                        Text(
                            text = myErrorMessage!!,
                            color = Color.Red,
                            modifier = Modifier
                                .fillMaxWidth()
                                .padding(8.dp)
                        )
                    }

                    Spacer(modifier = Modifier.height(16.dp))

                    Button(
                        onClick = {
                            if (isSignIn) {
                                signIn(auth, email, password,
                                    onSignedIn = { signedInUser ->
                                        onSignedIn(signedInUser)
                                    },
                                    onSignInError = { errorMessage ->
                                        Toast.makeText(applicationContext, errorMessage, Toast.LENGTH_LONG).show()
                                    }
                                )
                            } else {
                                signUp(auth, email, password, firstName, lastName, { signedInUser ->
                                    onSignedIn(signedInUser)
                                }) { signUpError ->
                                    Toast.makeText(applicationContext, signUpError, Toast.LENGTH_LONG).show()
                                }
                            }
                        },
                        modifier = Modifier
                            .fillMaxWidth()
                            .height(60.dp)
                            .padding(8.dp),
                    ) {
                        Text(
                            text = if (isSignIn) "Prijavi se" else "Registriraj se",
                            fontSize = 18.sp,
                            style = TextStyle(textDecoration = TextDecoration.Underline)
                        )
                    }

                    Box(
                        modifier = Modifier
                            .fillMaxWidth()
                            .height(50.dp)
                            .padding(8.dp),
                    ) {
                        ClickableText(
                            text = AnnotatedString(buildAnnotatedString {
                                withStyle(style = SpanStyle(color = Color.Blue)) {
                                    append(if (isSignIn) "Još nemaš račun? Registriraj se" else "Već imas račun? Prijavi se")
                                }
                            }.toString()),
                            onClick = {
                                myErrorMessage = null
                                email = ""
                                password = ""
                                isSignIn = !isSignIn
                            },
                            modifier = Modifier
                                .align(Alignment.Center)
                        )
                    }
                }
            }

        }
    }

    @OptIn(ExperimentalMaterial3Api::class)
    @Composable
    fun MainScreen(onSignOut: () -> Unit) {
        var showAddGameScreen by remember { mutableStateOf(false) }
        var showListScreen by remember { mutableStateOf(false) }
        val userProfile = remember { mutableStateOf<User?>(null) }
        var mainScreenOpened by remember { mutableStateOf(false) }
        var terminsList by remember {mutableStateOf<List<Termin>?>(null)}
        var activeTerminsList by remember {mutableStateOf<List<Termin>?>(null)}
        val currentUserID = auth.currentUser!!.uid
        val terminsCollection = db.collection("termini")
        var refreshUserTermins by remember { mutableStateOf(false) }
        var refreshActiveTermins by remember { mutableStateOf(false) }

        @Composable
        fun fetchTermins() {
            terminsList = fetchTerminsForCurrentUser()
            activeTerminsList = fetchActiveTerminsForCurrentUser()
        }
        LaunchedEffect(Unit) {
            mainScreenOpened = true
        }

        if (mainScreenOpened){
            fetchTermins()
        }

        LaunchedEffect(showListScreen, showAddGameScreen, refreshUserTermins){
            try {
                terminsCollection
                    .whereEqualTo("CreatedBy", db.collection("users").document(currentUserID))
                    .get()
                    .addOnSuccessListener { snapshot ->
                        val fetchedTermins = mutableListOf<Termin>()
                        for (doc in snapshot.documents) {
                            val termin = doc.toObject<Termin>()
                            if (termin != null) {
                                fetchedTermins.add(termin.copy(Id = doc.id))
                            }
                        }
                        terminsList = fetchedTermins
                        refreshUserTermins = false
                    }
                    .addOnFailureListener { exception ->
                        exception.printStackTrace()
                    }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

        LaunchedEffect(showListScreen, showAddGameScreen, refreshActiveTermins) {
            try {
                terminsCollection
                    .whereArrayContainsAny("igraci", listOf(currentUserID))
                    .get()
                    .addOnSuccessListener { snapshot ->
                        val fetchedTermins = mutableListOf<Termin>()
                        for (doc in snapshot.documents) {
                            val termin = doc.toObject<Termin>()
                            if (termin != null) {
                                fetchedTermins.add(termin.copy(Id = doc.id))
                            }
                        }
                        activeTerminsList = fetchedTermins
                        refreshActiveTermins = false
                    }
                    .addOnFailureListener { exception ->
                        exception.printStackTrace()
                    }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

        LaunchedEffect(auth.currentUser?.uid) {
            val userDocRef = db.collection("users").document(auth.currentUser!!.uid)

            userDocRef.get()
                .addOnSuccessListener { document ->
                    if (document.exists()) {
                        val firstName = document.getString("firstName")
                        val lastName = document.getString("lastName")

                        userProfile.value = User(firstName, lastName, auth.currentUser!!.email ?: "")
                    }
                }
                .addOnFailureListener { e ->
                }
        }

        Scaffold(
            topBar = {
                Surface(
                    modifier = Modifier.background(Color.Red),
                ) {
                    TopAppBar(
                        colors =
                            TopAppBarDefaults.topAppBarColors(
                                containerColor = Color(0xFFDDD5F3),
                            ),
                        title = {
                            userProfile.value?.let{
                                Text(
                                    "${userProfile.value!!.firstName} ${userProfile.value!!.lastName}",
                                    modifier = Modifier.fillMaxWidth(),
                                    style = Typography.bodyLarge,
                                    textAlign = TextAlign.Center,
                                    color = Color.White
                                )
                            }
                        },
                        actions = {
                            IconButton(onClick = { onSignOut() }) {
                                Icon(Icons.Filled.ExitToApp, contentDescription = "Logout")
                            }
                        }
                    )
                }
            },
            bottomBar = {
                BottomAppBar() {
                    Box (
                        modifier = Modifier.fillMaxWidth(),
                        contentAlignment = Alignment.Center,
                    ){
                        if (!showListScreen && !showAddGameScreen) {
                            FloatingActionButton(
                                modifier = Modifier.padding(end = 80.dp),
                                onClick = {
                                    showListScreen = true
                                    mainScreenOpened = false
                                }
                            ) {
                                Icon(
                                    imageVector = Icons.Default.List,
                                    contentDescription = "Add",
                                )
                            }
                        }
                        if (!showListScreen && !showAddGameScreen) {
                            FloatingActionButton(
                                modifier = Modifier.padding(start = 80.dp),
                                onClick = {
                                    showAddGameScreen = true
                                    mainScreenOpened = false
                                }
                            ) {
                                Icon(
                                    imageVector = Icons.Default.Add,
                                    contentDescription = "Add",
                                )
                            }
                        }
                    }
                }

                if (showAddGameScreen) {
                    AddGameScreen(
                        onClose = {
                            showAddGameScreen = false
                        }
                    )
                }

                if (showListScreen) {
                    TerminListScreen(
                        onClose = {
                            showListScreen = false
                        }
                    )
                }
            },
            content = {  innerPadding ->
                Column(
                    modifier = Modifier
                        .fillMaxSize()
                        .padding(innerPadding),
                    horizontalAlignment = Alignment.CenterHorizontally,
                    verticalArrangement = Arrangement.Top
                ) {
                    userProfile.value?.let {
                        TerminsSection(terminsList, onDelete = { refreshUserTermins = true  })
                        ActiveTerminsSection(activeTerminsList, onDelete = { refreshActiveTermins = true })
                    }
                }
            }
        )
    }

    @Composable
    fun TerminsSection(termins: List<Termin>?, onDelete: () -> Unit) {
        var openDialog by remember { mutableStateOf(false) }
        val selectedTermin = remember { mutableStateOf<Termin?>(null) }
        Box(
            modifier = Modifier
                .fillMaxWidth()
                .padding(horizontal = 16.dp, vertical = 8.dp)
                .heightIn(max = 300.dp),
            contentAlignment = Alignment.TopCenter
        ) {
            Column(
                modifier = Modifier.fillMaxSize(),
                verticalArrangement = Arrangement.Center,
                horizontalAlignment = Alignment.CenterHorizontally
            ) {
                Text(
                    text = "POSTAVLJENI TERMINI",
                    style = MaterialTheme.typography.titleLarge.copy(fontWeight = FontWeight.ExtraBold),
                    modifier = Modifier.padding(top = 20.dp, bottom = 16.dp)
                )
                Surface(
                    shape = RoundedCornerShape(16.dp),
                ) {
                    LazyRow(
                        modifier = Modifier.background(Color(0xFFEEF1EA)),
                        contentPadding = PaddingValues(horizontal = 4.dp, vertical = 8.dp),
                        horizontalArrangement = Arrangement.spacedBy(8.dp)
                    ) {
                        if (!termins.isNullOrEmpty()) {
                            items(termins) { termin ->
                                TerminBox(termin = termin, showDivider = true,
                                    onLongClick = {selectedTermin.value = it
                                        openDialog = true
                                    })
                            }
                        } else {
                            item {
                                Text(
                                    text = "NEMA POSTAVLJENIH TERMINA.",
                                    modifier = Modifier.padding(horizontal = 8.dp, vertical = 16.dp)
                                )
                            }
                        }
                    }
                }
            }
        }

        if (openDialog && selectedTermin != null) {
            AlertDialog(
                title = {
                    Text(text = "Želiš li obrisati odabrani termin?")
                },
                onDismissRequest = {
                    openDialog = false
                },
                confirmButton = {
                    TextButton(
                        onClick = {
                            openDialog = false
                            deleteTermin(selectedTermin.value!!)
                            onDelete()
                        }
                    ) {
                        Text("DA")
                    }
                },
                dismissButton = {
                    TextButton(
                        onClick = {
                            openDialog = false
                        }
                    ) {
                        Text("NE")
                    }
                }
            )
        }
    }

    @OptIn(ExperimentalFoundationApi::class)
    @Composable
    fun TerminBox(termin: Termin, showDivider: Boolean, onLongClick: (Termin?) -> Unit) {
        val terminDate = SimpleDateFormat("dd/MM/yyyy", Locale.getDefault()).format(termin.Date!!.toDate())
        val backgroundColor = if (hasPassed(terminDate)) {
            Color(0xFFFFCCCC)
        } else if (termin.NeededPlayers > 0) {
            Color(0xFF90EE90)
        } else {
            Color(0xFFFFFF99)
        }

        Box(
            modifier = Modifier
                .combinedClickable(
                    onClick = {},
                    onLongClick = {
                        onLongClick(termin)
                    }
                )
                .padding(vertical = 8.dp, horizontal = 8.dp)
                .fillMaxHeight()
                .background(
                    color = backgroundColor,
                    shape = RoundedCornerShape(10.dp)
                )
        ) {
            Column(
                modifier = Modifier.padding(16.dp)
            ) {
                Text(
                    text = terminDate,
                    style = MaterialTheme.typography.bodyMedium.copy(fontWeight = FontWeight.Bold),
                    modifier = Modifier
                        .padding(bottom = 4.dp)
                        .align(alignment = Alignment.CenterHorizontally)
                )
                Text(
                    text = termin.Sport,
                    style = Typography.bodyMedium.copy(fontWeight = FontWeight.Bold, textAlign = TextAlign.Center),
                    modifier = Modifier
                        .padding(bottom = 8.dp)
                        .align(alignment = Alignment.CenterHorizontally)
                )
                Divider(
                    color = Color.White,
                    modifier = Modifier
                        .padding(vertical = 10.dp)
                        .width(150.dp)
                        .height(1.dp)
                )
                Text(
                    text = "Vrijeme: ${termin.Time}",
                    style = Typography.bodySmall,
                    modifier = Modifier.padding(bottom = 4.dp)
                )
                Text(
                    text = "Adresa: ${termin.Address}",
                    style = Typography.bodySmall,
                    modifier = Modifier.padding(bottom = 4.dp)
                )
                Text(
                    text = "Broj potrebnih igrača: ${termin.NeededPlayers}",
                    style = Typography.bodySmall,
                    modifier = Modifier.padding(bottom = 4.dp)
                )
            }
        }

    }

    fun hasPassed(dateString: String): Boolean {
        val sdf = SimpleDateFormat("dd/MM/yyyy")
        val currentDate = Calendar.getInstance()
        val sdfDate = Calendar.getInstance()
        sdfDate.time = sdf.parse(dateString)

        currentDate.set(Calendar.HOUR_OF_DAY, 0)
        currentDate.set(Calendar.MINUTE, 0)
        currentDate.set(Calendar.SECOND, 0)
        currentDate.set(Calendar.MILLISECOND, 0)

        sdfDate.set(Calendar.HOUR_OF_DAY, 0)
        sdfDate.set(Calendar.MINUTE, 0)
        sdfDate.set(Calendar.SECOND, 0)
        sdfDate.set(Calendar.MILLISECOND, 0)

        return currentDate.after(sdfDate)
    }

    @Composable
    fun ActiveTerminsSection(termins: List<Termin>?, onDelete: () -> Unit) {
        var openDialog by remember { mutableStateOf(false) }
        val selectedTermin = remember { mutableStateOf<Termin?>(null) }
        Box(
            modifier = Modifier
                .fillMaxWidth()
                .padding(horizontal = 16.dp, vertical = 8.dp)
                .heightIn(max = 300.dp),
            contentAlignment = Alignment.TopCenter
        ) {
            Column(
                modifier = Modifier.fillMaxSize(),
                verticalArrangement = Arrangement.Center,
                horizontalAlignment = Alignment.CenterHorizontally
            ) {
                Text(
                    text = "PRIJAVLJENI TERMINI",
                    style = MaterialTheme.typography.titleLarge.copy(fontWeight = FontWeight.ExtraBold),
                    modifier = Modifier.padding(top = 20.dp, bottom = 16.dp)
                )
                Surface(
                    shape = RoundedCornerShape(16.dp),
                ) {
                    LazyRow(
                        modifier = Modifier.background(Color(0xFFEEF1EA)),
                        contentPadding = PaddingValues(horizontal = 4.dp, vertical = 8.dp),
                        horizontalArrangement = Arrangement.spacedBy(8.dp)
                    ) {
                        if (termins != null && termins.isNotEmpty()) {
                            items(termins) { termin ->
                                TerminBox(termin = termin, showDivider = true, onLongClick = {selectedTermin.value = it
                                    openDialog = true
                                })
                            }
                        } else {
                            item {
                                Text(
                                    text = "NEMA PRIJAVLJENIH TERMINA.",
                                    modifier = Modifier.padding(horizontal = 8.dp, vertical = 16.dp)
                                )
                            }
                        }
                    }
                }
            }
        }

        if (openDialog && selectedTermin != null) {
            AlertDialog(
                title = {
                    Text(text = "Želite li odjaviti odabrani termin?")
                },
                onDismissRequest = {
                    openDialog = false
                },
                confirmButton = {
                    TextButton(
                        onClick = {
                            openDialog = false
                            unsubscribeTermin(selectedTermin.value!!)
                            onDelete()
                        }
                    ) {
                        Text("DA")
                    }
                },
                dismissButton = {
                    TextButton(
                        onClick = {
                            openDialog = false
                        }
                    ) {
                        Text("NE")
                    }
                }
            )
        }
    }

    @OptIn(ExperimentalMaterial3Api::class)
    @Composable
    fun AddGameScreen(onClose: () -> Unit) {

        var playgroundName by remember { mutableStateOf("") }
        var selectedTime by remember { mutableStateOf("") }
        var missingPlayers by remember { mutableStateOf("") }
        var expanded by remember { mutableStateOf(false) }
        var selectedDate by remember { mutableStateOf(Timestamp.now()) }
        val sports = listOf("Košarka", "Nogomet", "Rukomet")
        var selectedSport by remember { mutableStateOf(sports[0]) }

        BackHandler(true) {
            onClose()
        }

        Column(
            modifier = Modifier
                .fillMaxSize()
                .padding(16.dp),
            verticalArrangement = Arrangement.Top,
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Row(
                verticalAlignment = Alignment.CenterVertically,
                modifier = Modifier.fillMaxWidth()
            ) {
                IconButton(onClick = onClose) {
                    Icon(Icons.Default.ArrowBack, contentDescription = "Back")
                }
                Text(text = "DODAJ TERMIN", fontSize = 24.sp, modifier = Modifier.padding(start = 40.dp))
            }

            Spacer(modifier = Modifier.height(16.dp))

            Column(
                modifier = Modifier
                    .fillMaxWidth()
            ) {
                ExposedDropdownMenuBox(
                    expanded = expanded,
                    onExpandedChange = {
                        expanded = !expanded
                    },
                    modifier = Modifier.fillMaxWidth()
                ) {
                    OutlinedTextField(
                        value = selectedSport,
                        onValueChange = {},
                        readOnly = true,
                        trailingIcon = { ExposedDropdownMenuDefaults.TrailingIcon(expanded = expanded) },
                        modifier = Modifier
                            .menuAnchor()
                            .fillMaxWidth()
                            .padding(vertical = 16.dp)
                            .border(1.dp, Color.Black, RoundedCornerShape(4.dp))
                            .clickable(onClick = { expanded = !expanded })
                    )

                    ExposedDropdownMenu(
                        expanded = expanded,
                        onDismissRequest = { expanded = false },
                    ) {
                        Column(
                        ) {
                            sports.forEach { item ->
                                DropdownMenuItem(
                                    text = { Text(text = item) },
                                    onClick = {
                                        selectedSport = item
                                        expanded = false
                                    },
                                    contentPadding = ExposedDropdownMenuDefaults.ItemContentPadding
                                )
                            }
                        }
                    }
                }
            }

            ShowDatePicker(context = LocalContext.current) { date ->
                selectedDate = date
            }

            Spacer(modifier = Modifier.height(16.dp))

            ShowTimePicker(context = LocalContext.current) { time ->
                selectedTime = time
            }

            Spacer(modifier = Modifier.height(16.dp))


            OutlinedTextField(
                value = playgroundName,
                onValueChange = { playgroundName = it },
                label = { Text("Adresa odigravanja: ") },
                modifier = Modifier.fillMaxWidth()
            )

            Spacer(modifier = Modifier.height(16.dp))

            OutlinedTextField(
                value = missingPlayers,
                onValueChange = {
                    if (it.isEmpty() || it.all { char -> char.isDigit() }) {
                        missingPlayers = it
                    }
                },
                label = { Text("Broj potrebnih igrača: ") },
                modifier = Modifier.fillMaxWidth(),
                keyboardOptions = KeyboardOptions.Default.copy(keyboardType = KeyboardType.Number),
                singleLine = true,
                maxLines = 1,
            )

            Spacer(modifier = Modifier.height(16.dp))

            Button(
                onClick = {
                    auth.currentUser?.uid?.let { uid ->
                        val termin = hashMapOf(
                            "Address" to playgroundName,
                            "Sport" to selectedSport,
                            "Time" to selectedTime,
                            "Date" to selectedDate,
                            "NeededPlayers" to missingPlayers.toInt(),
                            "CreatedBy" to db.document("users/$uid")
                        )

                        db.collection("termini")
                            .add(termin)
                            .addOnSuccessListener {
                                onClose()
                            }
                            .addOnFailureListener { e ->

                                Log.w(TAG, "Error adding document", e)

                            }
                    }
                },
                modifier = Modifier.fillMaxWidth()
            ) {
                Text("Dodaj Termin")
            }

        }
    }

    @Composable
    fun ShowDatePicker(context: Context, onDateSelected: (Timestamp) -> Unit) {
        val calendar = Calendar.getInstance()
        val day = calendar.get(Calendar.DAY_OF_MONTH)
        val month = calendar.get(Calendar.MONTH)
        val year = calendar.get(Calendar.YEAR)
        val date = remember { mutableStateOf("") }

        val datePickerDialog = remember {
            android.app.DatePickerDialog(
                context,
                { _: DatePicker, year: Int, month: Int, day: Int ->
                    val selectedDate = Calendar.getInstance().apply {
                        set(Calendar.YEAR, year)
                        set(Calendar.MONTH, month)
                        set(Calendar.DAY_OF_MONTH, day)
                        set(Calendar.HOUR_OF_DAY, 0)
                        set(Calendar.MINUTE, 0)
                        set(Calendar.SECOND, 0)
                        set(Calendar.MILLISECOND, 0)
                    }

                    val timestamp = Timestamp(selectedDate.time)
                    val formattedDate = SimpleDateFormat("dd/MM/yyyy", Locale.getDefault()).format(selectedDate.time)
                    date.value = formattedDate
                    onDateSelected(timestamp)
                },
                year,
                month,
                day
            )
        }


        Box(modifier = Modifier.fillMaxWidth()) {
            OutlinedTextField(
                value = date.value,
                onValueChange = {},
                label = { Text("Izaberi Datum") },
                modifier = Modifier.fillMaxWidth()
            )
            IconButton(
                onClick = { datePickerDialog.show() },
                modifier = Modifier
                    .align(Alignment.CenterEnd)
                    .padding(top = 10.dp)

            ) {
                Icon(
                    Icons.Default.DateRange,
                    contentDescription = "Izaberi Datum"
                )
            }
        }
    }

    @Composable
    fun ShowTimePicker(context: Context, onTimeSelected: (String) -> Unit) {
        val calendar = Calendar.getInstance()
        val hour = calendar.get(Calendar.HOUR_OF_DAY)
        val minute = calendar.get(Calendar.MINUTE)

        val time = remember { mutableStateOf("") }

        val timePickerDialog = remember {
            TimePickerDialog(
                context,
                { _, hourOfDay, minute ->
                    val formattedTime = String.format("%02d:%02d", hourOfDay, minute)
                    time.value = formattedTime
                    onTimeSelected(formattedTime)
                },
                hour,
                minute,
                true
            )
        }

        Box(modifier = Modifier.fillMaxWidth()) {
            OutlinedTextField(
                value = time.value,
                onValueChange = {},
                label = { Text("Izaberi Vrijeme") },
                modifier = Modifier.fillMaxWidth()
            )
            IconButton(
                onClick = { timePickerDialog.show() },
                modifier = Modifier
                    .align(Alignment.CenterEnd)
                    .padding(top = 10.dp)
            ) {
                Icon(
                    Icons.Default.Create,
                    contentDescription = "Izaberi Vrijeme"
                )
            }
        }
    }

    private fun signIn(
        auth: FirebaseAuth,
        email: String,
        password: String,
        onSignedIn: (FirebaseUser) -> Unit,
        onSignInError: (String) -> Unit
    ) {
        if (email.isBlank() || password.isBlank()) {
            onSignInError("Email and password must not be empty")
            return
        }

        auth.signInWithEmailAndPassword(email, password)
            .addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    val user = auth.currentUser
                    onSignedIn(user!!)
                } else {
                    val errorMessage = task.exception?.message ?: "Invalid email or password"
                    onSignInError(errorMessage)
                }
            }
    }

    private fun signUp(
        auth: FirebaseAuth,
        email: String,
        password: String,
        firstName: String,
        lastName: String,
        onSignedIn: (FirebaseUser) -> Unit,
        onSignUpError: (String) -> Unit
    ) {
        if (email.isBlank() || password.isBlank() || firstName.isBlank() || lastName.isBlank()) {
            onSignUpError("All fields must be filled out")
            return
        }

        auth.createUserWithEmailAndPassword(email, password)
            .addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    val user = auth.currentUser

                    val userProfile = hashMapOf(
                        "firstName" to firstName,
                        "lastName" to lastName,
                        "email" to email
                    )

                    db.collection("users")
                        .document(user!!.uid)
                        .set(userProfile)
                        .addOnSuccessListener {
                            onSignedIn(user)
                        }
                        .addOnFailureListener { e ->
                            onSignUpError(e.message ?: "Failed to save user profile")
                        }
                } else {
                    val errorMessage = task.exception?.message ?: "Registration failed"
                    onSignUpError(errorMessage)
                }
            }
    }

    @Composable
    fun fetchTerminsForCurrentUser(): List<Termin> {
        var terminsList by remember { mutableStateOf<List<Termin>>(emptyList()) }

        val currentUserID = auth.currentUser!!.uid
        val terminsCollection = db.collection("termini")

        LaunchedEffect(currentUserID) {
            try {
                terminsCollection
                    .whereEqualTo("CreatedBy", db.collection("users").document(currentUserID))
                    .get()
                    .addOnSuccessListener { snapshot ->
                        val fetchedTermins = mutableListOf<Termin>()
                        for (doc in snapshot.documents) {
                            val termin = doc.toObject<Termin>()
                            if (termin != null) {
                                fetchedTermins.add(termin.copy(Id = doc.id))
                            }
                        }
                        terminsList = fetchedTermins
                    }
                    .addOnFailureListener { exception ->
                        exception.printStackTrace()
                    }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

        return terminsList
    }

    @Composable
    fun fetchActiveTerminsForCurrentUser(): List<Termin> {
        var activeTerminsList by remember { mutableStateOf<List<Termin>>(emptyList()) }

        val currentUserID = auth.currentUser!!.uid
        val terminsCollection = db.collection("termini")

        LaunchedEffect(currentUserID) {
            try {
                terminsCollection
                    .whereArrayContainsAny("igraci", listOf(currentUserID))
                    .get()
                    .addOnSuccessListener { snapshot ->
                        val fetchedTermins = mutableListOf<Termin>()
                        for (doc in snapshot.documents) {
                            val termin = doc.toObject<Termin>()
                            if (termin != null) {
                                fetchedTermins.add(termin.copy(Id = doc.id))
                            }
                        }
                        activeTerminsList = fetchedTermins
                    }
                    .addOnFailureListener { exception ->
                        exception.printStackTrace()
                    }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

        return activeTerminsList
    }

    @Composable
    fun fetchTermins(): List<TerminModel> {
        val terminsList = remember { mutableStateOf<List<TerminModel>>(emptyList()) }
        val terminsCollection = db.collection("termini")
        LaunchedEffect(Unit) {
            try {
                terminsCollection
                    .whereGreaterThan("NeededPlayers", 0)
                    .get()
                    .addOnSuccessListener { snapshot ->
                        val fetchedTermins = mutableListOf<TerminModel>()
                        for (doc in snapshot.documents) {
                            var termin = doc.toObject<TerminModel>()
                            if (termin != null) {
                                val terminDate = SimpleDateFormat("dd/MM/yyyy", Locale.getDefault()).format(termin.Date!!.toDate())
                                termin.Id = doc.id
                                val userId = termin.CreatedBy!!.id
                                if (userId != auth.currentUser!!.uid && !hasPassed(terminDate) && !termin.igraci.contains(auth.currentUser!!.uid)) {
                                    db.collection("users").document(userId)
                                        .get()
                                        .addOnSuccessListener { userDoc ->
                                            val firstName = userDoc.getString("firstName") ?: ""
                                            val lastName = userDoc.getString("lastName") ?: ""
                                            val user = User(firstName, lastName, null)
                                            fetchedTermins.add(termin.copy(User = user))
                                            terminsList.value = fetchedTermins
                                        }
                                }
                            }
                        }
                    }
                    .addOnFailureListener { exception ->
                        exception.printStackTrace()
                    }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

        return terminsList.value
    }



    @OptIn(ExperimentalMaterial3Api::class)
    @Composable
    fun TerminListScreen(onClose: () -> Unit) {
        var isLoading by remember { mutableStateOf(true) }
        var terminsList: List<TerminModel>? = null

        fetchTermins()?.let {
            terminsList = it
            isLoading = false
        }

        if (isLoading){
            LoadingScreen()
        }
        else {
            BackHandler(true) {
                onClose()
            }

            Column(
                modifier = Modifier
                    .fillMaxSize()
                    .padding(16.dp),
                verticalArrangement = Arrangement.Top,
                horizontalAlignment = Alignment.CenterHorizontally
            ) {
                // Title
                Row(
                    verticalAlignment = Alignment.CenterVertically,
                    modifier = Modifier.fillMaxWidth()
                ) {
                    IconButton(onClick = onClose) {
                        Icon(Icons.Default.ArrowBack, contentDescription = "Back")
                    }
                    Text(
                        text = "SLOBODNI TERMINI",
                        fontSize = 24.sp,
                        modifier = Modifier.padding(start = 40.dp)
                    )
                }

                Spacer(modifier = Modifier.height(16.dp))

                Box(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(horizontal = 16.dp, vertical = 8.dp)
                        .fillMaxHeight(),
                    contentAlignment = Alignment.TopCenter
                ) {
                    Column(
                        modifier = Modifier.fillMaxSize(),
                        verticalArrangement = Arrangement.Center,
                        horizontalAlignment = Alignment.CenterHorizontally
                    ) {
                        Surface(
                            shape = RoundedCornerShape(16.dp),
                        ) {
                            Column(
                                modifier = Modifier
                                    .background(Color(0xFFEEF1EA))
                                    .verticalScroll(rememberScrollState())
                            ) {
                                if (terminsList != null && terminsList!!.isNotEmpty()) {
                                    terminsList!!.forEach { termin ->
                                        TerminListBox(
                                            termin = termin,
                                            showDivider = true,
                                            onSuccess = { success ->
                                                if (success) {
                                                    onClose()
                                                }
                                            }
                                        )
                                    }
                                } else {
                                    Text(
                                        text = "NEMA SLOBODNIH TERMINA",
                                        modifier = Modifier.padding(
                                            horizontal = 8.dp,
                                            vertical = 16.dp
                                        )
                                    )
                                }
                            }
                        }
                    }
                }

            }
        }
    }

    fun deleteTermin(termin: Termin) {
        val terminRef = FirebaseFirestore.getInstance().collection("termini").document(termin.Id)
        terminRef.delete()
            .addOnSuccessListener {
            }
            .addOnFailureListener { exception ->
                exception.printStackTrace()
            }
    }

    fun unsubscribeTermin(termin: Termin) {
        val terminRef = FirebaseFirestore.getInstance().collection("termini").document(termin.Id)
        val currentUserId = auth.currentUser!!.uid

        val updatedIgraci = termin.igraci.toMutableList().apply { remove(currentUserId) }

        val updatedNeededPlayers = termin.NeededPlayers + 1

        val updates = mapOf(
            "igraci" to updatedIgraci,
            "NeededPlayers" to updatedNeededPlayers
        )

        terminRef.update(updates)
            .addOnSuccessListener {
            }
            .addOnFailureListener { exception ->
                exception.printStackTrace()
            }
    }


    @OptIn(ExperimentalFoundationApi::class)
    @Composable
    fun TerminListBox(termin: TerminModel, showDivider: Boolean, onSuccess: (Boolean) -> Unit) {
        val terminDate = SimpleDateFormat("dd/MM/yyyy", Locale.getDefault()).format(termin.Date!!.toDate())
        Box(
            modifier = Modifier
                .padding(vertical = 8.dp, horizontal = 8.dp)
                .fillMaxWidth()
                .background(
                    color = Color(0xFF90EE90),
                    shape = RoundedCornerShape(10.dp)
                )
        ) {
            Column(
                modifier = Modifier.padding(16.dp)
            ) {
                Text(
                    text = terminDate,
                    style = MaterialTheme.typography.bodyMedium.copy(fontWeight = FontWeight.Bold),
                    modifier = Modifier.padding(bottom = 4.dp)
                )
                Text(
                    text = termin.Sport,
                    style = Typography.bodyMedium.copy(fontWeight = FontWeight.Bold, textAlign = TextAlign.Center),
                    modifier = Modifier.padding(bottom = 8.dp)
                )
                Divider(
                    color = Color.White,
                    modifier = Modifier
                        .padding(vertical = 10.dp)
                        .fillMaxWidth()
                        .height(1.dp)
                )
                Text(
                    text = "Vrijeme: ${termin.Time}",
                    style = Typography.bodySmall,
                    modifier = Modifier.padding(bottom = 4.dp)
                )
                Text(
                    text = "Adresa: ${termin.Address}",
                    style = Typography.bodySmall,
                    modifier = Modifier.padding(bottom = 4.dp)
                )
                Text(
                    text = "Broj potrebnih igrača: ${termin.NeededPlayers}",
                    style = Typography.bodySmall,
                    modifier = Modifier.padding(bottom = 4.dp)
                )
                Text(
                    text = "Kreirao: ${termin.User!!.firstName} ${termin.User!!.lastName}",
                    style = Typography.bodySmall,
                    modifier = Modifier.padding(bottom = 4.dp)
                )
                Button(
                    onClick = { updateTerminAndAddUser(termin, onSuccess ) },
                    modifier = Modifier.align(Alignment.CenterHorizontally)
                ) {
                    Text(text = "IGRAJ TERMIN")
                }

            }
        }
    }

    fun updateTerminAndAddUser(termin: TerminModel, onSuccess: (Boolean) -> Unit) {
        val terminRef = FirebaseFirestore.getInstance().collection("termini").document(termin.Id)
        val currentUserId = auth.currentUser!!.uid

        val newNeededPlayers = termin.NeededPlayers - 1

        terminRef.get()
            .addOnSuccessListener { terminSnapshot ->
                val igraci = terminSnapshot.get("igraci") as? ArrayList<String> ?: ArrayList()
                igraci.add(currentUserId)

                terminRef.update(
                    mapOf(
                        "NeededPlayers" to newNeededPlayers,
                        "igraci" to igraci
                    )
                )
                    .addOnSuccessListener {
                        onSuccess(true)
                    }
                    .addOnFailureListener { exception ->
                        exception.printStackTrace()
                    }
            }
            .addOnFailureListener { exception ->
                exception.printStackTrace()
            }
    }


}

data class User(
    val firstName: String?,
    val lastName: String?,
    val email: String?
)

data class Termin(
    val Id: String = "",
    val Date: Timestamp? = null,
    val CreatedBy: DocumentReference? = null,
    val Sport: String = "",
    val Time: String = "",
    val Address: String = "",
    val NeededPlayers: Int = 0,
    val igraci: List<String> = emptyList()
)

data class TerminModel(
    var Id: String = "",
    val Date: Timestamp? = null,
    val CreatedBy: DocumentReference? = null,
    val Sport: String = "",
    val Time: String = "",
    val Address: String = "",
    val NeededPlayers: Int = 0,
    val User: User? = null,
    val igraci: List<String> = emptyList()
)



